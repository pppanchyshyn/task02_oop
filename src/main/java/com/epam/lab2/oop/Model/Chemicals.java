package com.epam.lab2.oop.Model;

public class Chemicals implements Catalog {

  protected String type = "Chemicals";
  protected String name;
  protected String format;
  protected double price;
  protected int capacity;


  public Chemicals() {
  }

  public Chemicals(String name, String format, double price, int capacity) {
    this.name = name;
    this.format = format;
    this.price = price;
    this.capacity = capacity;
  }

  public String getSubType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public String getFormat() {
    return format;
  }

  public double getPrice() {
    return price;
  }

  public int getCapacity() {
    return capacity;
  }

  public void getInfo() {
    System.out.printf("%-22s%-14s%-6.1f%-10s%-5d%-10s%-10s%n", getSubType(), getName(), getPrice(),
        "UAH", getCapacity(), "ml", getFormat());
  }
}
