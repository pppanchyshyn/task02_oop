package com.epam.lab2.oop.Model;

public class Glassware extends Chemicals {

  protected String subType = "Glassware";

  public Glassware() {
  }

  public Glassware(String name, String format, double price, int capacity) {
    super(name, format, price, capacity);
  }

  @Override
  public String getSubType() {
    return subType;
  }
}
