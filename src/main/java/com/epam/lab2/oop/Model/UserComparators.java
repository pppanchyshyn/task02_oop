package com.epam.lab2.oop.Model;

import java.util.Comparator;

public class UserComparators {

  public static Comparator<Chemicals> byPrice = Comparator.comparing(x -> x.getPrice());
  public static Comparator<Chemicals> byCapacity = Comparator.comparing(x -> x.getCapacity());
  public static Comparator<Chemicals> byName = Comparator.comparing(x -> x.getName());
}

