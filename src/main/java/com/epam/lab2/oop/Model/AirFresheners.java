package com.epam.lab2.oop.Model;

public class AirFresheners extends Chemicals {

  protected String subType = "Air freshers";

  public AirFresheners() {
  }

  public AirFresheners(String name, String format, double price, int capacity) {
    super(name, format, price, capacity);
  }

  @Override
  public String getSubType() {
    return subType;
  }
}
