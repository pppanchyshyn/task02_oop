package com.epam.lab2.oop.Model;

import java.util.ArrayList;
import java.util.Comparator;

public class DataBase {

  public ArrayList<Chemicals> catalog = new ArrayList();

  public DataBase() {
    catalog.add(new AirFresheners("Glade", "Spray", 54.8, 400));
    catalog.add(new AirFresheners("Glade", "Spray", 32.5, 300));
    catalog.add(new AirFresheners("Glade", "Spray", 84.3, 250));
    catalog.add(new AirFresheners("Alpen", "Spray", 22.9, 300));
    catalog.add(new AirFresheners("Ozone", "Spray", 22.5, 300));
    catalog.add(new AirFresheners("Ozone", "Spray", 22.5, 300));
    catalog.add(new Glassware("Mr Muscle", "Spray", 41, 500));
    catalog.add(new Glassware("Clin", "Spray", 33.9, 500));
    catalog.add(new Glassware("Clin", "Aqua", 177, 4500));
    catalog.add(new Glassware("Mr Muscle", "Aqua", 27.8, 500));
    catalog.add(new Glassware("Astonish", "Aqua", 94.1, 750));
    catalog.add(new Glassware("Domol", "Spray", 37.4, 750));
    catalog.add(new BathroomCleaners("Mr Muscle", "Spray", 63.9, 750));
    catalog.add(new BathroomCleaners("Domestos", "Aqua", 42.9, 1000));
    catalog.add(new BathroomCleaners("Sarma", "Aqua", 39.9, 750));
    catalog.add(new BathroomCleaners("Domol", "Aqua", 59.9, 500));
    catalog.add(new BathroomCleaners("Domestos", "Aqua", 84, 2000));
    catalog.add(new BathroomCleaners("Domol", "Aqua", 36, 200));
  }

  public void getCatalog() {
    catalog.stream().forEach(x -> x.getInfo());
  }

  public void getSubCatalog(String type) {
    catalog.stream().filter(x -> x.getSubType().equalsIgnoreCase(type)).forEach(x -> x.getInfo());
  }

  public void sortCatalog(String type, Comparator<Chemicals> comparator) {
    catalog.stream().filter(x -> x.getSubType().equalsIgnoreCase(type)).sorted(comparator)
        .forEach(x -> x.getInfo());
  }
}
