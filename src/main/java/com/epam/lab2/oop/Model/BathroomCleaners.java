package com.epam.lab2.oop.Model;

public class BathroomCleaners extends Chemicals {

  protected String subType = "Bathroom cleaners";

  public BathroomCleaners() {
  }

  public BathroomCleaners(String name, String format, double price, int capacity) {
    super(name, format, price, capacity);
  }

  @Override
  public String getSubType() {
    return subType;
  }
}
