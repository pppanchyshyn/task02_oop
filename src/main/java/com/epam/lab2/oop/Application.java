package com.epam.lab2.oop;

import com.epam.lab2.oop.Controller.Controller;

public class Application {

  public static void main(String[] args) {
    Controller controller = new Controller();
    controller.execute();
  }
}
