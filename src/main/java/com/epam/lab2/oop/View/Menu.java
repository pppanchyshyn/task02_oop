package com.epam.lab2.oop.View;

import java.util.Scanner;

public class Menu {

  private Scanner input = new Scanner(System.in);

  private int enterInteger() {
    int integer;
    while (true) {
      if (input.hasNextInt()) {
        integer = input.nextInt();
        break;
      } else {
        System.out.print("Not integer! Try again: ");
        input.nextLine();
      }
    }
    return integer;
  }

  public int enterMenuItem(int exitValue) {
    int menuItem;
    while (true) {
      menuItem = enterInteger();
      if ((menuItem > 0) && (menuItem <= exitValue)) {
        break;
      } else {
        System.out.print("Non-existent menu item. Try again: ");
        input.nextLine();
      }
    }
    return menuItem;
  }

  public static void printMainMenu(){
    System.out.println("Welcome to our store"
        + "\nTo review all positions press 1"
        + "\nTo review air freshers press 2"
        + "\nTo review bathroom cleaners press 3"
        + "\nTo review glassware press 4"
        + "\nTo leave store press 5"
        + "\nEnter your choice: ");
  }

  public static void printSortingMenu(){
    System.out.println("Choose sorting type"
        + "\nSorting by brand press 1"
        + "\nSorting by price $-$$$ press 2"
        + "\nSorting by price $$$-$ press 3"
        + "\nSorting by capacity c-ccc press 4"
        + "\nSorting by capacity ccc-c press 5"
        + "\nGet back to main menu press 6");
  }
}
