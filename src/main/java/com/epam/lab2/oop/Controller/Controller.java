package com.epam.lab2.oop.Controller;

import com.epam.lab2.oop.Model.Constants;
import com.epam.lab2.oop.View.Menu;
import com.epam.lab2.oop.Model.DataBase;
import com.epam.lab2.oop.Model.UserComparators;

public class Controller {

  private DataBase dataBase = new DataBase();
  private Menu menu = new Menu();

  private void executeSortMenu(String type) {
    int sortMenuExitValue = 0;
    while (!(sortMenuExitValue == Constants.SORT_MENU_EXIT_VALUE)) {
      Menu.printSortingMenu();
      int sortingMenuItem = menu.enterMenuItem(Constants.SORT_MENU_EXIT_VALUE);
      switch (sortingMenuItem) {
        case 1:
          dataBase.sortCatalog(type, UserComparators.byName);
          break;
        case 2:
          dataBase.sortCatalog(type, UserComparators.byPrice);
          break;
        case 3:
          dataBase.sortCatalog(type, UserComparators.byPrice.reversed());
          break;
        case 4:
          dataBase.sortCatalog(type, UserComparators.byCapacity);
          break;
        case 5:
          dataBase.sortCatalog(type, UserComparators.byCapacity.reversed());
          break;
        case 6:
          sortMenuExitValue = Constants.SORT_MENU_EXIT_VALUE;
          break;
      }
    }
  }

  public void execute() {
    int exitValue = 0;
    while (!(exitValue == Constants.EXIT_VALUE)) {
      Menu.printMainMenu();
      int mainMenuItem = menu.enterMenuItem(Constants.EXIT_VALUE);
      switch (mainMenuItem) {
        case 1:
          dataBase.getCatalog();
          break;
        case 2:
          dataBase.getSubCatalog("Air freshers");
          executeSortMenu("Air freshers");
          break;
        case 3:
          dataBase.getSubCatalog("Bathroom cleaners");
          executeSortMenu("Bathroom cleaners");
          break;
        case 4:
          dataBase.getSubCatalog("Glassware");
          executeSortMenu("Glassware");
          break;
        case 5:
          exitValue = Constants.EXIT_VALUE;
          break;
      }
    }
  }
}
